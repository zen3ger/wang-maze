use std::fs::File;
use std::io::{self, Write};
use std::process::exit;

// Image constant
const TILE_WIDTH: usize = 16;
const TILE_HEIGHT: usize = 16;

const ROWS: usize = 60;
const COLS: usize = 60;

const WIDTH: usize = COLS * TILE_WIDTH;
const HEIGHT: usize = ROWS * TILE_HEIGHT;

// PPM helpers
fn ppm_header(f: &mut File, w: usize, h: usize) -> Result<(), io::Error> {
    let hdr = format!("P6\n{} {}\n255\n", w, h);
    f.write(hdr.as_bytes()).map(|_| ())
}

fn create_ppm(f: &str, w: usize, h: usize) -> File {
    let mut ppm = match File::create(f) {
        Err(e) => {
            eprintln!("ERROR: failed to create {}: {}", f, e);
            exit(1);
        },
        Ok(ppm) => ppm,
    };

    if let Err(e) = ppm_header(&mut ppm, w, h) {
        eprintln!("ERROR: failed to write PPM header for {}: {}", f, e);
        exit(1);
    }

    ppm
}

fn write_pixel(f: &mut File, p: &Pixel) {
    if let Err(e) = f.write(p) {
        eprintln!("ERROR: failed to write pixel data: {}", e);
        exit(1);
    }
}

// "Shader"
const R: usize = 0;
const G: usize = 1;
const B: usize = 2;

type Pixel = [u8; 3];

const BLACK: Pixel = [0, 0, 0];
const WHITE: Pixel = [255, 255, 255];

fn uv(x: usize, y: usize, w: usize, h: usize) -> (f32, f32) {
    (x as f32 / w as f32,
     y as f32 / h as f32)
}

fn maze(u: f32, v: f32, mask: u8) -> Pixel {
    let dist = |center: (f32, f32), r: f32| -> f32 {
        let dx = center.0 - u;
        let dy = center.1 - v;
        r - (dx*dx + dy*dy).sqrt()
    };

    if mask == 0 {
        return BLACK;
    }

    if dist((0.5, 0.5), 0.2) > 0.0 {
        return WHITE;
    }

    if is_set(mask, RIGHT)
        && (0.6 .. 1.0).contains(&v)
        && (0.4 .. 0.6).contains(&u) {
        WHITE
    } else if is_set(mask, LEFT)
        && (0.0 .. 0.4).contains(&v)
        && (0.4 .. 0.6).contains(&u) {
        WHITE
    } else if is_set(mask, TOP)
        && (0.0 .. 0.4).contains(&u)
        && (0.4 .. 0.6).contains(&v) {
        WHITE
    } else if is_set(mask, BOTTOM)
        && (0.6 .. 1.0).contains(&u)
        && (0.4 .. 0.6).contains(&v) {
        WHITE
    } else {
        BLACK
    }
}

// Coordinate transformations
fn to_tile_space(x: usize, y: usize) -> (f32, f32) {
    let tx = x % TILE_WIDTH;
    let ty = y % TILE_HEIGHT;

    uv(tx, ty, TILE_WIDTH, TILE_HEIGHT)
}

fn to_tile_id(x: usize, y: usize) -> (usize, usize) {
    let col = x / TILE_WIDTH;
    let row = y / TILE_HEIGHT;

    (col, row)
}

const BOTTOM: u8 = 0x8;
const RIGHT: u8 = 0x4;
const LEFT: u8 = 0x2;
const TOP: u8 = 0x1;

fn is_set(tile: u8, side: u8) -> bool {
    tile & side == side
}

fn tile() -> [[u8; COLS]; ROWS] {
    use rand::Rng;

    let mut ts = [[0; COLS]; ROWS];
    let mut rng = rand::thread_rng();

    for row in 0..ROWS {
        for col in 0..COLS {
            ts[row][col] = loop {
                let t = rng.gen_range(0..16);

                if col > 0 && is_set(ts[row][col - 1], BOTTOM) != is_set(t, TOP) {
                       continue;
                }
                if row > 0  && is_set(ts[row - 1][col], RIGHT) != is_set(t, LEFT) {
                        continue;
                }

                break t;
            };
        }
    }

    ts
}

fn main() {
    let mut ppm = create_ppm("wang-maze.ppm", WIDTH, HEIGHT);
    let tiles = tile();

    for x in 0..WIDTH {
        for y in 0..HEIGHT {
            let (u, v) = uv(x, y, WIDTH, HEIGHT);

            let (c, r) = to_tile_id(x, y);
            let (tu, tv) = to_tile_space(x, y);

            let mask = tiles[r][c];
            let pixel = maze(tu, tv, mask);

            let pixel = [
                (pixel[R] as f32 * (1.0 - u)) as u8,
                (pixel[G] as f32 * (1.0 - v)) as u8,
                0
            ];
            write_pixel(&mut ppm, &pixel);
        }
    }
}

#[allow(dead_code)]
fn maze_tiles() {
    for mask in 0..=15 {
        let file_name = format!("tile-{:02}.ppm", mask);
        let mut ppm = create_ppm(&file_name, TILE_WIDTH, TILE_HEIGHT);

        for x in 0..TILE_WIDTH {
            for y in 0..TILE_HEIGHT {
                let (u, v) = uv(x, y, TILE_WIDTH, TILE_HEIGHT);
                let pixel = maze(u, v, mask);
                write_pixel(&mut ppm, &pixel);
            }
        }
    }
}
