# [Wang](https://en.wikipedia.org/wiki/Wang_tile) tiling based maze generator

![Image of a 3-by-3 maze](./.readme-img.png "3x3 Wang Maze")

This is simple "maze" generator written *for fun* using the rules of 2-colour Wang dominoes.
The maze is renderred on CPU to a [PPM](https://en.wikipedia.org/wiki/Netpbm#File_formats) image.

```console
$ cargo run --release
$ feh ./wang-maze.ppm
```
